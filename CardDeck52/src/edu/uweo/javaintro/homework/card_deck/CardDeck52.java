package edu.uweo.javaintro.homework.card_deck;


public class CardDeck52 {
	private final boolean[] cards = new boolean[52];
	
	private static final int
			RANK_JACK = 11,
			RANK_QUEEN = 12,
			RANK_KING = 13,
			RANK_ACE = 14,
			SUIT_HEARTS = 0,
			SUIT_CLUBS = 1,
			SUIT_DIAMONDS = 2,
			SUIT_SPADES = 3;

	public CardDeck52() {
		super();
	}
	
	public CardDeck52( StandardCard...standardCards ) {
		super();
		for ( StandardCard card : standardCards )
			this.cards[getCardPosition(card)] = true;
	}
	
	public void dealCard( StandardCard card ) {
		int position = getCardPosition(card);
		cards[position] = true;
	}
	
//	returns true if the card is dealt, false if not
//	throws IllegalArgumentException if the card doesn't make sense
	public boolean isCardDealt ( StandardCard card ) {
		int position = getCardPosition(card);
		return cards[position];
	}
	
//	returns the position of the rank in relation to the suit
//	throws IllegalArgumentException if the rank isn't recognized
	private static int getRankPosition( String rank ) {
		try {
			int rankInt = Integer.parseInt(rank);
			if (rankInt < 0 || rankInt > 8)
				throw new IllegalArgumentException();
			return rankInt;
		} catch (IllegalArgumentException e) {
			switch (rank) {
			case "Jack":
				return RANK_JACK;
			case "Queen":
				return RANK_QUEEN;
			case "King":
				return RANK_KING;
			case "Ace":
				return RANK_ACE;
			default:
				throw new IllegalArgumentException("Rank must be 2-10, "
						+ "'Jack', 'Queen', 'King', or 'Ace'");
			}
		}
	}
	
//	overloaded convenience method for getRankPosition(String)
	private static int getRankPosition( StandardCard card ) {
		return getRankPosition( card.getRank() );
	}

//	returns the position of the suit
//	throws IllegalArgumentException if the suit isn't recognized
	private static int getSuitPosition( String suit ) {
		switch (suit) {
		case "Hearts":
			return SUIT_HEARTS;
		case "Clubs":
			return SUIT_CLUBS;
		case "Diamonds":
			return SUIT_DIAMONDS;
		case "Spades":
			return SUIT_SPADES;
		default:
			throw new IllegalArgumentException("suit must be in 'Hearts',"
					+ " 'Clubs', 'Diamonds', or 'Spades'");
		}
	}
	
//	overloaded convenience method for getSuitPosition(String)
	private static int getSuitPosition( StandardCard card ) {
		return getSuitPosition(card.getSuit());
	}
	
//	returns absolute position of the card in the deck
	private static int getCardPosition( StandardCard card ) {
		int suit = getSuitPosition(card) * 13;
		int rank = getRankPosition(card) - 2;
		
		return suit + rank;
	}
}

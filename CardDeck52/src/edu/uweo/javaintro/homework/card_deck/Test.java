package edu.uweo.javaintro.homework.card_deck;

public class Test {
	
	private static final String[] RANKS = {
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"10",
		"Jack",
		"Queen",
		"King",
		"Ace",
		"1",
		"11"
	};
	
	private static final String[] SUITS = {
		"Hearts",
		"Clubs",
		"Diamonds",
		"Spades",
		"hearts",
		"Cluvs"
	};
	
	public static void main(String[] args) {
		testCardDeck52();
	}
	
	public static void testCardDeck52() {
		
		CardDeck52 deck = new CardDeck52();

		for (int suitPosition = 0; 
				suitPosition < SUITS.length; 
				suitPosition++) {
			for (int rankPosition = 0;
					rankPosition < RANKS.length;
					rankPosition++) {
				StandardCard card =
						new StandardCard(RANKS[rankPosition],
								SUITS[suitPosition]);
				
				try {
					System.out.printf("%s: %b%n", card.toString(),
							deck.isCardDealt(card));
					
					deck.dealCard(card);
					
					System.out.println("\t" + deck.isCardDealt(card));
					
				} catch (IllegalArgumentException e) {
					
					System.out.printf("%s: %s%n",
							card.toString(), e.getMessage());
					
				}
			}
		}
	}
	
}

package edu.uweo.javaintro.homework.card_deck;

public class StandardCard
{
    private final String  rank_;
    private final String  suit_;
    
    public StandardCard( String rank, String suit )
    {
        rank_ = rank;
        suit_ = suit;
    }
    
    public String getRank()
    {
        return rank_;
    }
    
    public String getSuit()
    {
        return suit_;
    }
    
    @Override
    public String toString()
    {
        String  result  = rank_ + " of " + suit_;
        return result;
    }
}
